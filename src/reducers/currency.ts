import { ICurrencyState } from './types';
import {LOAD_CURRENCY_SUCCESS, SET_ERROR, SET_FETCHING, SET_FILTERED_LIST} from '../actions/types';

export const initState: ICurrencyState = {
    list: [],
    filteredList: [],
    loading: false,
    error: null,
};

export function currencyReducer(state: ICurrencyState = initState, action: any): ICurrencyState {
    switch (action.type) {
        case LOAD_CURRENCY_SUCCESS: {
            return {
                ...state,
                list: action.list,
                filteredList: action.list,
            };
        }
        case SET_FILTERED_LIST: {
            return {
                ...state,
                filteredList: action.list,
            };
        }
        case SET_ERROR: {
            return {
                ...state,
                error: action.payload,
            };
        }
        case SET_FETCHING: {
            return {
                ...state,
                loading: action.isFetching,
            };
        }
        default:
            return state;
    }
}
