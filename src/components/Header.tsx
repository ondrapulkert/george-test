import React from 'react';

const Header: React.FC<{}> = () => {
    return (
        <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <div className="container">
                <h3 className="my-0 mr-md-auto font-weight-normal">George FE Test</h3>
            </div>
        </div>
    );
};

export default Header;
