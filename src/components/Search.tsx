import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUrlSearchParams } from 'use-url-search-params';
import { ICurrency } from '../interfaces/currency.interface';
import { AppStore } from '../reducers';
import { setFilteredList } from '../actions';

const types: TypesType = {
    term: String,
};

const Search: React.FC<{}> = () => {
    const [isFixed, setFixed] = useState(false);
    const ref = useRef<HTMLDivElement>(null);
    const [query, setQuery] = useState<string>('');
    const data: ICurrency[] = useSelector((state: AppStore) => state.currency.list);
    const [params, setParams] = useUrlSearchParams({ term: '' }, types);
    const dispatch = useDispatch();

    const handleScroll = () => {
        if (ref.current) {
            window.scrollY > ref.current.getBoundingClientRect().bottom ? setFixed(true) : setFixed(false);
        }
    };

    useEffect(() => {
        const handleSearch = (query: string) => {
            if (query) {
                const term = query.toLowerCase();
                const results = data.filter(
                    item => item?.currency?.toLowerCase().includes(term) || item?.nameI18N?.toLowerCase().includes(term)
                );
                dispatch(setFilteredList(results));
            } else {
                dispatch(setFilteredList(data));
            }
        };

        const timeout = setTimeout(() => {
            handleSearch(query);
        }, 500);

        return () => {
            clearTimeout(timeout);
        };
    }, [query, dispatch, data]);

    useEffect(() => {
        if (params.term) {
            setQuery(params.term.toString());
        }
    }, [params.term]);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', () => handleScroll);
        };
    }, []);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = event.target.value;
        setQuery(value);
        setParams({ term: value });
    };

    return (
        <div className={`wrapper${isFixed ? ' fixed' : ''}`} ref={ref} data-testid="wrapper">
            <div className="container">
                <div className="row no-gutters">
                    <div className="col-md-4">
                        <div className="form-group has-search">
                            <span className="fa fa-search form-control-feedback"></span>
                            <input
                                value={query}
                                type="text"
                                className="form-control"
                                placeholder="Search"
                                onChange={handleInputChange}
                                data-testid="input"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Search;
