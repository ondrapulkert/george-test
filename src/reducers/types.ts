import { ICurrency } from '../interfaces/currency.interface';

export interface ICurrencyState {
    list: ICurrency[];
    filteredList: ICurrency[];
    loading: boolean;
    error: any;
}
