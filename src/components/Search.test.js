import React from 'react';
import Search from './Search';
import { fireEvent } from '@testing-library/react';
import { render } from '../test/testUtils';

describe('<Search /> with other props', () => {
    it('should set the value for input on change event', () => {
        const { getByTestId } = render(<Search />);
        const input = getByTestId('input');
        fireEvent.change(input, { target: { value: 'Test value' } });
        expect(input.value).toBe('Test value');
    });
});
