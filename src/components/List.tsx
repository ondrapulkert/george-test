import React from 'react';
import { ICurrency } from '../interfaces/currency.interface';
import { useSelector } from 'react-redux';
import { AppStore } from '../reducers';
import ListItem from './ListItem';

const List: React.FC<{}> = () => {
    const data: ICurrency[] = useSelector((state: AppStore) => state.currency.filteredList);
    const loading: boolean = useSelector((state: AppStore) => state.currency.loading);

    if (loading) {
        return (
            <div className="loader-wrapper">
                <div className="loader">&nbsp;</div>
            </div>
        );
    }

    if (data.length <= 0) {
        return (
            <div className="card">
                <div className="card-body">No data found</div>
            </div>
        );
    }

    return (
        <div className="container">
            <table className="table table-striped">
                <tbody>
                    {data.map((item: ICurrency, index: number) => {
                        return <ListItem item={item} key={index} />;
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default List;
