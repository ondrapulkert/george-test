import { ICurrency } from './currency.interface';

export interface IResponseCurrencyInterface {
    institute: number;
    lastUpdated: string;
    comparisonDate: string;
    baseCurrency: string;
    fx: ICurrency[];
}
