import {ICurrency} from "../interfaces/currency.interface";

export const LOAD_CURRENCY_SUCCESS = 'LOAD_CURRENCY_SUCCESS';
export const SET_ERROR = 'SET_ERROR';
export const SET_FETCHING = 'SET_FETCHING';
export const SET_FILTERED_LIST = 'SET_FILTERED_LIST';

export interface SetFetching {
    type: typeof SET_FETCHING;
    isFetching: boolean;
}

export interface LoadCurrencyListSuccess {
    type: typeof LOAD_CURRENCY_SUCCESS;
    list: ICurrency[];
}

export interface SetFilteredList {
    type: typeof SET_FILTERED_LIST;
    list: ICurrency[];
}

export interface SetError {
    type: typeof SET_ERROR;
    payload: any;
}

export type CurrencyActionTypes =
    | SetFetching
    | LoadCurrencyListSuccess
    | SetError
    | SetFilteredList;
