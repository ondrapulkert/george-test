import { currencyReducer, initState } from './currency';
import { LOAD_CURRENCY_SUCCESS, SET_ERROR, SET_FETCHING, SET_FILTERED_LIST } from '../actions/types';
import { mockData } from '../test/mockData';

describe('currency reducer', () => {
    it('should return the initial state', () => {
        expect(currencyReducer(undefined, {})).toEqual(initState);
    });

    it('should handle LOAD_CURRENCY_SUCCESS', () => {
        const newState = currencyReducer(undefined, {
            type: LOAD_CURRENCY_SUCCESS,
            list: mockData,
        });
        expect(newState).toStrictEqual({ ...initState, list: mockData, filteredList: mockData });
    });

    it('should handle SET_FILTERED_LIST', () => {
        const newState = currencyReducer(undefined, {
            type: SET_FILTERED_LIST,
            list: mockData,
        });
        expect(newState).toStrictEqual({ ...initState, filteredList: mockData });
    });

    it('should handle SET_FETCHING', () => {
        const failAction = {
            type: SET_FETCHING,
            isFetching: true,
        };
        expect(currencyReducer(initState, failAction)).toEqual({ ...initState, loading: true });
    });

    it('should handle SET_ERROR', () => {
        const failAction = {
            type: SET_ERROR,
            payload: { message: 'Error', success: false },
        };
        expect(currencyReducer(initState, failAction)).toEqual({
            ...initState,
            error: { message: 'Error', success: false },
        });
    });
});
