import { Dispatch } from 'redux';
import axios from 'axios';
import {ICurrency} from "../interfaces/currency.interface";
import {
    CurrencyActionTypes,
    LOAD_CURRENCY_SUCCESS,
    SET_ERROR,
    SET_FETCHING,
    SET_FILTERED_LIST
} from "./types";
import {IResponseCurrencyInterface} from "../interfaces/responseCurrency.interface";

export const isFetching = (isFetching: boolean): CurrencyActionTypes => {
    return { type: SET_FETCHING, isFetching };
};

export const loadCurrencyListSuccess = (list: IResponseCurrencyInterface): CurrencyActionTypes => {
    return {
        type: LOAD_CURRENCY_SUCCESS,
        list: list.fx,
    };
};

export const setError = (error: any): CurrencyActionTypes => {
    return {
        type: SET_ERROR,
        payload: error,
    };
};

export const setFilteredList = (list: ICurrency[]): CurrencyActionTypes => {
    return {
        type: SET_FILTERED_LIST,
        list,
    };
};

export const loadCurrencyList = (): any => {
    return async (dispatch: Dispatch) => {
        dispatch(isFetching(true));
        try {
            const response = await axios.get(`data/fx.json`);
            const data = await response.data;
            dispatch(loadCurrencyListSuccess(data));
            dispatch(isFetching(false));
            return data;

        } catch (error) {
            dispatch(isFetching(false));
            dispatch(setError(error));
        }
    };
};
