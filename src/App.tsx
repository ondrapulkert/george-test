import React, { useEffect } from 'react';
import Header from './components/Header';
import Search from './components/Search';
import { useDispatch } from 'react-redux';
import { loadCurrencyList } from './actions';
import './App.css';
import List from './components/List';

function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadCurrencyList());
    }, [dispatch]);

    return (
        <>
            <Header />
            <div className="search-holder">
                <Search />
            </div>
            <List />
        </>
    );
}

export default App;
