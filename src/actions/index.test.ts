import moxios from 'moxios';
import { isFetching, loadCurrencyList, loadCurrencyListSuccess } from './index';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { mockDataResponse } from '../test/mockData';

const middlewares = [thunk];

const mockStore = configureMockStore(middlewares);

describe('loadCurrencyList action creator', () => {
    beforeEach(() => {
        moxios.install();
    });
    afterEach(() => {
        moxios.uninstall();
    });

    it('successful api request calls loadCurrencyList action', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: mockDataResponse,
            });
        });

        const expectedActions = [isFetching(true), loadCurrencyListSuccess(mockDataResponse), isFetching(false)];
        const store = mockStore({});

        return store.dispatch(loadCurrencyList()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
