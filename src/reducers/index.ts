import { combineReducers } from 'redux';
import { currencyReducer } from './currency';
import { ICurrencyState } from './types';

export interface AppStore {
    currency: ICurrencyState;
}

const rootReducer = combineReducers({
    currency: currencyReducer,
});

export default rootReducer;
