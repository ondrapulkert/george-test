import { ICurrency } from '../interfaces/currency.interface';
import { IResponseCurrencyInterface } from '../interfaces/responseCurrency.interface';

export const mockData: ICurrency[] = [
    {
        currency: 'FJD',
        precision: 2,
        nameI18N: 'Fiji Dollar',
        exchangeRate: {
            buy: 2,
            middle: 2.25,
            sell: 2.5,
            indicator: 0,
            lastModified: '2012-02-14T23:00:00Z',
        },
        banknoteRate: {
            buy: 2,
            middle: 2.25,
            sell: 2.5,
            indicator: 0,
            lastModified: '2012-02-14T23:00:00Z',
        },
        flags: ['provided'],
    },
];

export const mockDataResponse: IResponseCurrencyInterface = {
    institute: 20,
    lastUpdated: 'update',
    comparisonDate: 'comparision',
    baseCurrency: 'EUR',
    fx: mockData,
};
