import React from 'react';
import { ICurrency } from '../interfaces/currency.interface';
import './styles.css';

interface ListItemProps {
    item: ICurrency;
}

const ListItem: React.FC<ListItemProps> = ({ item: { currency, nameI18N, exchangeRate, banknoteRate } }) => {
    const flag = currency
        .trim()
        .substring(0, 2)
        .toLowerCase();

    const renderImage = () => {
        try {
            const image = require(`../../public/flags/${flag}.png`);
            return <img src={image} alt={image} />;
        } catch (e) {}
    };

    const showExchange = () => {
        if (exchangeRate) {
            return exchangeRate.middle;
        } else if (banknoteRate) {
            return banknoteRate.middle;
        } else {
            return <span>No exchange rate found</span>;
        }
    };

    return (
        <>
            <tr>
                <td>
                    {renderImage()}
                    {nameI18N}
                </td>
                <td>{currency.trim() ? currency : <span>No currency found</span>}</td>
                <td>{showExchange()}</td>
            </tr>
        </>
    );
};

export default ListItem;
