export interface ICurrency {
    banknoteRate?: {
        buy: number;
        indicator: number;
        lastModified: string;
        middle: number;
        sell: number;
    };
    denominations?: string[];
    currency: string;
    exchangeRate?: {
        buy: number;
        indicator: number;
        lastModified: string;
        middle: number;
        sell: number;
    };
    flags: string[];
    nameI18N?: string;
    precision?: number;
}